# Logging

This package is a fork of the package found at [https://github.com/google/exposure-notifications-server/tree/main/pkg/logging](https://github.com/google/exposure-notifications-server/tree/main/pkg/logging).

## Overview

[This article](https://kubernetes.io/blog/2020/09/04/kubernetes-1-19-introducing-structured-logs/) does an excellent job of describing the importance of structured logs.

Log aggregating platforms tend to prefer structured logs in JSON since parsing them is easier. However, for local development and better human readability, programmers tend to prefer structured logs in plain text.

The [original package](https://github.com/google/exposure-notifications-server/tree/main/pkg/logging) provides structured logs in either JSON or plain text format, is simple and easy to use.

Uber's [zap logger](https://pkg.go.dev/go.uber.org/zap) is used under the hood.

Tweaks/modifications have been me made to the original package to automatically pipe logs to STDOUT or STDERR based on the severity level.

Finally, the intention of this fork is to create an easier way to import the [original package](https://github.com/google/exposure-notifications-server/tree/main/pkg/logging) without running into errors.

## Features

- Ability to specify min log level, defaults to WARN, (example usage: LOG_LEVEL=ERROR)
- Ability to specify console encoding vs JSON encoding
- DEBUG and INFO logs are written to STDOUT
- All other log levels (WARN, ERROR, DPANIC, PANIC, FATAL) are written to STDERR 

## Example usage

```go
// main.go
package main

import (
	"gitlab.com/ownageoss/logging"
	"go.uber.org/zap"
)

type server struct {
	logger *zap.SugaredLogger
}

func main() {
	instance := &server{}

	instance.logger = logging.NewLoggerFromEnv()

	instance.logger.Debug("debug message...")

	instance.logger.Infof("%s", "info message...")

	instance.logger.Warn("warning message...")

	instance.logger.Errorf("%s", "error message...")
}
```

Run as follows for console encoding:

```sh
LOG_LEVEL=DEBUG LOG_MODE=DEV go run main.go
```

Run as follows for JSON encoding:

```sh
LOG_LEVEL=INFO go run main.go
```

## Versioning

This project uses [Semantic Versioning 2.0.0](https://semver.org/).

## Project status

This library is used in production environments and is actively maintained.

## Supported Go versions

Go version support is aligned with the Go's [release policy](https://go.dev/doc/devel/release#policy).

## Software Bill of Materials (SBOM)

```
               _      
 ___ _ __   __| |_  __
/ __| '_ \ / _` \ \/ /
\__ \ |_) | (_| |>  < 
|___/ .__/ \__,_/_/\_\
    |_|               

 📂 SPDX Document gitlab.com/ownageoss/logging
  │ 
  │ 📦 DESCRIBES 1 Packages
  │ 
  ├ logging
  │  │ 🔗 13 Relationships
  │  ├ CONTAINS FILE README.md (README.md)
  │  ├ CONTAINS FILE .gitlab-ci.yml (.gitlab-ci.yml)
  │  ├ CONTAINS FILE Makefile (Makefile)
  │  ├ CONTAINS FILE .gitignore (.gitignore)
  │  ├ CONTAINS FILE go.sum (go.sum)
  │  ├ CONTAINS FILE go.mod (go.mod)
  │  ├ CONTAINS FILE LICENSE (LICENSE)
  │  ├ CONTAINS FILE logging.png (logging.png)
  │  ├ CONTAINS FILE logger.go (logger.go)
  │  ├ CONTAINS FILE logger_test.go (logger_test.go)
  │  ├ CONTAINS FILE logging.spdx (logging.spdx)
  │  ├ DEPENDS_ON PACKAGE go.uber.org/zap@v1.27.0
  │  └ DEPENDS_ON PACKAGE go.uber.org/multierr@v1.11.0
  │ 
  └ 📄 DESCRIBES 0 Files
```