module gitlab.com/ownageoss/logging

go 1.23.0

require (
	github.com/google/go-cmp v0.6.0
	go.uber.org/zap v1.27.0
)

require go.uber.org/multierr v1.11.0 // indirect
